# obilet.com Front-end Case

## Installation
```
// 1 Clone this repo
git clone https://hakanyucelsen@bitbucket.org/hakanyucelsen/obilet-frontend-case.git

// 2 Into the repo directory
cd obilet-frontend-case

// 3 Install dependency
npm install

// 4 Get started
npm run server // start node server for obilet api
npm run serve // start client application
```

## Requirements
[Node JS](https://nodejs.org/en/), npm (installed with Node JS) and [Gulp CLI](https://gulpjs.com/) globally must be installed to use this starter kit.
```
// Install gulp globally
npm install -g gulp
```
