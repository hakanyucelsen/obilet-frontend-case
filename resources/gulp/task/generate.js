const args = require('yargs').argv;
const rename = require('gulp-rename');
const template = require('gulp-template');
const {gulp} = require('../config');
const inquirer = require('inquirer');
const componentPath = 'resources/assets/components/';
const types = ['Components', 'Page', 'Javascript'];

const create = () => {
    inquirer
      .prompt([
          {
              type: 'list', name: 'type', message: 'What type of item will you generate?', choices: types,
          }, {
              type: 'input', name: 'name', message: "What is item name?",
          },])
      .then((answers) => {
          switch (answers.type) {
              case 'Components': {
                  generateComponent(answers.name);
                  createJSFile(answers.name);
                  break;
              }
              case 'Page': {
                  generatePage(answers.name);
                  break;
              }
              case 'Javascript': {
                  generateJavascript(answers.name);
                  break;
              }
          }
      });
}

const createJSFile = (name) => {
    inquirer
      .prompt([
          {
              type: 'list', name: 'js', message: 'Include Javascript file in your component?', choices: [
                  'Yes', 'No',],
          }
      ])
      .then((answers) => {
          if (answers.js === 'Yes') {
              generateJSFileInComponent(name);
          }
      });
}

const generatePage = (name) => {
    return gulp.src('./resources/gulp/template/pug-page.template')
      .pipe(rename(name + '.pug'))
      .pipe(template({
          name: name
      }))
      .pipe(gulp.dest('resources/assets/pug'));
}

const generateComponent = (name) => {
    gulp.src('./resources/gulp/template/pug-component.template')
      .pipe(rename(name + '.pug'))
      .pipe(template({
          name: name
      }))
      .pipe(gulp.dest(componentPath + name));
    gulp.src('./resources/gulp/template/scss.template')
      .pipe(rename(name + '.scss'))
      .pipe(template({
          name: name
      }))
      .pipe(gulp.dest(componentPath + name));
}

const generateJavascript = (name) => {
    gulp.src('./resources/gulp/template/js.template')
      .pipe(rename(name + '.js'))
      .pipe(template({
          name: name.replace(/-([a-z])/g, function (g) {
              return g[1].toUpperCase();
          })
      }))
      .pipe(gulp.dest('resources/assets/js'));
}

const generateJSFileInComponent = (name) => {
    gulp.src('./resources/gulp/template/js.template')
      .pipe(rename(name + '.js'))
      .pipe(template({
          name: name.replace(/-([a-z])/g, function (g) {
              return g[1].toUpperCase();
          })
      }))
      .pipe(gulp.dest(componentPath + name));
}

gulp.task('g', () => {
    return create();
});