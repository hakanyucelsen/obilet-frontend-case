const {path, sync, gulp, url, virtualhost} = require("../config");

gulp.task('serve', (done) => {
    let opts = {};
    if (virtualhost) {
        opts = {
            notify: false,
            proxy: url,
            host: url,
            open: 'external',
            files: [
                './resources/views/**/*.blade.php'
            ]
        }
    } else {
        opts = {
            server: {
                baseDir: "./public"
            },
        }
    }
    sync.init(opts);

    gulp.watch(path.watchPath.scss, gulp.series('scss'));
    gulp.watch(path.watchPath.pug, gulp.series('pug'));
    gulp.watch(path.watchPath.js, gulp.series('app.js'));
    gulp.watch(path.source.images, gulp.series('minify.img'));
    gulp.watch(path.source.svg, gulp.series('icon.svg'));

    done();
});
