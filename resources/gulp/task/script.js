const {path, sync, gulp} = require("../config");
const concat = require('gulp-concat');

gulp.task('main.js', () => {
    if (path.node_modules.length > 0) {
        return gulp.src(path.node_modules)
          .pipe(concat('main.js'))
          .pipe(gulp.dest(path.dist.js))
          .pipe(sync.stream());
    } else {
        return null;
    }
});

gulp.task('app.js', () => {
    return gulp.src([
      'resources/assets/js/index.js',
      'resources/assets/js/api.js',
      'resources/assets/js/location.js',
      'resources/assets/js/date.js',
      'resources/assets/js/search-form.js',
      'resources/assets/js/result.js',
    ])
        // .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.dist.js))
        .pipe(sync.stream());
});
