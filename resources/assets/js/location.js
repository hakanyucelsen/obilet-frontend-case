const locationJS = function () {
    const formDom = document.querySelector('[data-search-form]');
    const allLocationSelect = document.querySelectorAll('[data-search-location-select]');

    // default selected locations
    const locationIST = {
        id: 349,
        name: 'İstanbul Avrupa',
    };
    const locationANK = {
        id: 356,
        name: 'Ankara'
    };

    const selectedLocation = {
        origin: {
            id: null,
            name: null
        },
        destination: {
            id: null,
            name: null
        }
    };
    let selectedLocationDirection;

    this.getSelectedLocation = () => {
        return selectedLocation;
    }

    const getLocation = (locationListDom, searchParam = null) => {
        locationListDom.classList.add('loading');
        __apiJS.getBusLocations(searchParam).then(
          (res) => {
              const locationList = res.data.slice(0, 6);

              if (locationList.length > 0) {
                  locationListDom.innerHTML = '';
                  locationList.reverse().forEach((item) => {
                      locationListDom.insertAdjacentHTML('afterbegin', '<div class="location-list__item" data-location-item="' + item.name + '" data-id="' + item.id + '">' + item.name + '</div>')
                  })
              }
          }
        ).catch(
          (error) => {
              alert(error);
          }
        ).finally(() => {
            locationListDom.classList.remove('loading');
        })
    }

    const setLocation = (data, direction) => {
        selectedLocation[direction] = data;
        document.querySelector('#' + direction + '-location').value = data.name;
        document.querySelector('#' + direction + '-location').setAttribute('data-name', data.name);
    }

    const showLocationSelect = (select) => {
        const input = select.querySelector('[data-search-location-input]');
        const locationListDom = select.querySelector('[data-location-list]')

        select.classList.add('active');
        input.value = null;

        if (locationListDom.childElementCount === 0) {
            getLocation(locationListDom);
        }

    }

    const hideLocationSelect = (select) => {
        const input = select.querySelector('[data-search-location-input]');

        select.classList.remove('active');
        input.value = input.getAttribute('data-name');
    }

    const clickLocationSelect = () => {
        allLocationSelect.forEach((select) => {
            select.addEventListener('click', (e) => {
                if (!select.classList.contains('active')) {
                    showLocationSelect(e.currentTarget);
                    formDom.classList.add('has-dropdown');
                    selectedLocationDirection = select.getAttribute('data-search-location-select');
                }
            })
        });
    }

    const clickLocationItem = () => {
        document.addEventListener('click', (e) => {
            if (e.target.getAttribute('data-location-item')) {
                setLocation({
                    id: e.target.getAttribute('data-id'),
                    name: e.target.getAttribute('data-location-item')
                }, selectedLocationDirection);
                hideLocationSelect(document.querySelector('[data-search-location-select="' + selectedLocationDirection + '"]'));
            }
        })
    }

    const clickOutDom = () => {
        window.addEventListener('click', (e) => {
            allLocationSelect.forEach(function (select) {
                if (!select.contains(e.target) && select.classList.contains('active')) {
                    hideLocationSelect(select);
                }
            });

            if (document.querySelector('[data-search-location-select].active') === null) {
                formDom.classList.remove('has-dropdown');
            }
        });
    }

    const clickSwap = () => {
        document.querySelector('[data-swap-location]').addEventListener('click', () => {
            const oldOrigin = selectedLocation.origin;
            const oldDestination = selectedLocation.destination;

            setLocation(oldDestination, 'origin');
            setLocation(oldOrigin, 'destination');
        })
    }

    this.search = (e) => {
        const select = e.closest('[data-search-location-select]');
        const locationListDom = select.querySelector('[data-location-list]')

        locationListDom.innerHTML = null;
        getLocation(locationListDom, e.value);
    }

    this.init = () => {
        clickOutDom();
        clickLocationSelect();
        clickLocationItem();
        clickSwap();

        if (__appJS.hasSearchData()) {
            const searchData = __appJS.getSearchData()
            setLocation(searchData.origin, 'origin')
            setLocation(searchData.destination, 'destination')

        } else {
            setLocation(locationIST, 'origin');
            setLocation(locationANK, 'destination');
        }
    }

};
const __locationJS = new locationJS();