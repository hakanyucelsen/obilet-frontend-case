const API_URL = 'https://v2-api.obilet.com/api';
const opts = {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic REd3cEIia3spV295VVc3G2FtVjNZbWx1',
    }
}

const app = function () {
    const config = {
        language: 'tr',
        apiLanguage: 'tr-TR',
        localeDateFormat: {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'}
    }
    const session = sessionStorage.getItem('device-session');

    this.getConfig = () => {
        return config;
    }

    this.getSearchData = () => {
        const data = localStorage.getItem('search');
        if (data) {
            return JSON.parse(data);
        } else {
            return null;
        }
    }

    this.hasSearchData = () => {
        return !!localStorage.getItem('search');
    }

    this.init = () => {
        __searchFormJS.init();
        __locationJS.init();
        __dateJS.init();

        if (!session) {
            __apiJS.getSession().then(
              (response) => {
                  sessionStorage.setItem('device-session', JSON.stringify({
                      'session-id': response.data['session-id'],
                      'device-id': response.data['device-id']
                  }))
              }
            )
        }
    }
}

const __appJS = new app();
