const searchFormJS = function () {
    const formDom = document.querySelector('[data-search-form]')
    let formData = null;

    const submitForm = () => {
        formDom.addEventListener('submit', function (e) {
            e.preventDefault();

            const searchData = __locationJS.getSelectedLocation();
            searchData['departureDate'] = __dateJS.getSelectedDate();
            formData = searchData;

            try {
                sameLocationValidation()
            } catch (error) {
                alert(error);
                return false;
            }

            try {
                minDepartureDateValidation()
            } catch (error) {
                alert(error);
                return false;
            }

            localStorage.setItem('search', JSON.stringify(searchData));
            window.location.href = './list.html';
        })
    }

    const sameLocationValidation = () => {
        if (formData.origin.id === formData.destination.id) {
            throw 'Can not select same location as both origin and destination';
        }
    }

    const minDepartureDateValidation = () => {
        const departureDate = new Date(formData.departureDate);
        const currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);

        if (currentDate.getTime() - departureDate.getTime() > 0) {
            throw 'Minimum valid date for departure date is Today.';
        }
    }

    this.init = () => {
        submitForm();
    }
};
const __searchFormJS = new searchFormJS();