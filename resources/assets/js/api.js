const apiJS = function () {
    const API_URL = 'http://localhost:3003';

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Basic JEcYcEMyantZV095WVc3G2JtVjNZbWx1");

    const reqOpts = {
        method: 'POST',
        headers: myHeaders
    }

    const config = __appJS.getConfig();

    this.getSession = () => {
        reqOpts['body'] = JSON.stringify({
            "type": 1,
            "connection": {
                "ip-address": "165.114.41.21",
                "port": "5117"
            },
            "browser": {
                "name": "Chrome",
                "version": "47.0.0.12"
            }
        });

        return new Promise(((resolve, reject) => {
            return fetch(API_URL + '/getsession', reqOpts)
              .then(response => response.json())
              .then(result => {
                  resolve(result)
              })
              .catch(error => {
                  reject(error)
              });
        }))
    };

    this.getBusLocations = (searchParam) => {
        reqOpts['body'] = JSON.stringify({
            "data": searchParam,
            "device-session": JSON.parse(sessionStorage.getItem('device-session')),
            "date": new Date().toISOString(),
            "language": config.apiLanguage
        });

        return new Promise(((resolve, reject) => {
            return fetch(API_URL + '/getbuslocation', reqOpts)
              .then(response => response.json())
              .then(result => {
                  resolve(result)
              })
              .catch(error => {
                  reject(error)
              });
        }))
    };

    this.getJourneys = (data) => {
        reqOpts['body'] = JSON.stringify({
            "device-session": JSON.parse(sessionStorage.getItem('device-session')),
            "date": new Date().toISOString(),
            "language": config.apiLanguage,
            "data": data
        });

        return new Promise(((resolve, reject) => {
            return fetch(API_URL + '/getjourneys', reqOpts)
              .then(response => response.json())
              .then(result => {
                  resolve(result)
              })
              .catch(error => {
                  reject(error)
              });
        }))
    }


};
const __apiJS = new apiJS();