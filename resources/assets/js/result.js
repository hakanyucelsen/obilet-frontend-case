const resultJS = function () {
    const config = __appJS.getConfig();
    const searchData = __appJS.getSearchData();
    const searchResultDom = document.querySelector('[data-search-result]');

    const setHeader = () => {
        document.querySelector('[data-location]').textContent = `${searchData.origin.name} - ${searchData.destination.name}`;
        document.querySelector('[data-date]').textContent = `${new Date(searchData.departureDate).toLocaleDateString(config.language, config.localeDateFormat)}`;
    }

    const getJourneys = () => {
        searchResultDom.classList.add('loading');
        __apiJS.getJourneys({
            'origin-id': searchData.origin.id,
            'destination-id': searchData.destination.id,
            'departure-date': searchData.departureDate,
        }).then(
          (res) => {
              res.data.reverse().forEach((item) => {
                  const departureTime = new Date(item.journey.departure).toTimeString().split(':');
                  const arrivalTime = new Date(item.journey.arrival).toTimeString().split(':');

                  searchResultDom.insertAdjacentHTML('afterbegin', `
                    <div class="search-result__item">
                      <div class="search-result__top">
                        <div class="search-result__time">
                          <div class="search-result__time-item">
                            <div class="search-result__time-label">Kalkış</div>
                            <div class="search-result__time-value">${departureTime[0]}:${departureTime[1]}</div>
                          </div><i class="fi fi-arrow-right search-result__time-icon"></i>
                          <div class="search-result__time-item">
                            <div class="search-result__time-label">Varış</div>
                            <div class="search-result__time-value">${arrivalTime[0]}:${arrivalTime[1]}</div>
                          </div>
                        </div>
                        <div class="search-result__price">${item.journey['internet-price']} ${item.journey.currency}</div>
                      </div>
                      <div class="search-result__destination">${item.journey.origin} - ${item.journey.destination}</div>
                    </div>
                  `)
              })
          }
        ).finally(
          () => {
              searchResultDom.classList.remove('loading');
          }
        )
    }

    this.init = () => {
        setHeader();
        getJourneys();
    };

};
const __resultJS = new resultJS();