const dateJS = function () {
    const config = __appJS.getConfig();
    let selectedDate = null;
    let datepicker = null;
    const departureInputDom = document.querySelector('#departure-date');
    const departurePickerDom = document.querySelector('[data-departure-picker]');
    const datepickerSelect = document.querySelector('[data-search-datepicker-select]')
    const localeDateStringOpts = config.localeDateFormat;

    const today = new Date();
    const tomorrow = new Date();
    today.setHours(0, 0, 0, 0);
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow.setHours(0, 0, 0, 0);

    const initDatepicker = () => {
        datepicker = new Datepicker(departurePickerDom, {
            minDate: 'today',
        });

        departurePickerDom.addEventListener('changeDate', (e) => {
            setDate(datepicker.getDate());
            datepickerSelect.closest('.search-form__group').classList.remove('active');
        })
    }

    const formatDate = (date) => {
        let month = '' + (date.getMonth() + 1);
        let day = '' + date.getDate();
        let year = date.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    const setDate = (date) => {
        departureInputDom.value = date.toLocaleDateString(config.language, localeDateStringOpts);
        selectedDate = formatDate(date);
        datepicker.setDate(date);

        document.querySelectorAll('[data-quick-date-btn]').forEach(function (item) {
            item.classList.remove('active');
        })
        switch (date.getTime()) {
            case tomorrow.getTime(): {
                document.querySelector('[data-quick-date-btn="tomorrow"]').classList.add('active');
                break;
            }
            case today.getTime(): {
                document.querySelector('[data-quick-date-btn="today"]').classList.add('active');
                break;
            }
        }
    }

    const clickDatepickerSelect = () => {
        datepickerSelect.addEventListener('click', function (e) {
            const parent = e.target.closest('.search-form__group');
            if (!parent.classList.contains('active')) {
                parent.classList.add('active');
            }
        });

        window.addEventListener('click', function (e) {
            if (!datepickerSelect.closest('.search-form__group').contains(e.target)) {
                datepickerSelect.closest('.search-form__group').classList.remove('active');
            }
        })
    }

    this.setQuickDate = (day = 'tomorrow') => {
        if (day === 'today') {
            setDate(today)
        } else if (day === 'tomorrow') {
            setDate(tomorrow)
        }
    }

    this.getSelectedDate = () => {
        return selectedDate;
    }

    this.init = () => {
        initDatepicker();
        clickDatepickerSelect();

        if (__appJS.hasSearchData()) {
            const searchData = __appJS.getSearchData();
            const date = new Date(searchData.departureDate);
            date.setHours(0, 0, 0, 0);
            setDate(date);
        } else {
            this.setQuickDate();
        }
    }
};
const __dateJS = new dateJS();