const express = require('express');
const axios = require('axios');
const cors = require('cors');
const bodyParser = require('body-parser')
const app = express();
const port = 3003;

const API_URL = 'https://v2-api.obilet.com/api';
const API_CLIENT_TOKEN = 'JEcYcEMyantZV095WVc3G2JtVjNZbWx1';

axios.defaults.baseURL = API_URL;
axios.defaults.headers.common['Authorization'] = 'Basic ' + API_CLIENT_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/json';

app.use(
  cors({
      credentials: true,
      origin: '*',
      preflightContinue: true,
  }),
);

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

app.post('/getsession', (req, res) => {
    const urlPath = '/client/getsession';
    const data = {
        "type": 1,
        "connection": {
            "ip-address": "165.114.41.21",
            "port": "5117"
        },
        "browser": {
            "name": "Chrome",
            "version": "47.0.0.12"
        }
    };

    axios.post(urlPath, JSON.stringify(data)).then(
      (response) => {
          res.send(response.data);
      }
    ).catch(
      (error) => {
          res.send(error);
      }
    )
})

app.post('/getbuslocation', (req, res) => {
    const urlPath = '/location/getbuslocations';
    axios.post(urlPath, JSON.stringify(req.body)).then(
      (response) => {
          res.send(response.data);
      }
    ).catch(
      (error) => {
          res.send(error);
      }
    )
})

app.post('/getjourneys', (req, res) => {
    const urlPath = '/journey/getbusjourneys';
    axios.post(urlPath, JSON.stringify(req.body)).then(
      (response) => {
          res.send(response.data);
      }
    ).catch(
      (error) => {
          res.send(error);
      }
    )
})

app.listen(port, () => {
    console.log(`Server start on http://localhost:${port}`)
})